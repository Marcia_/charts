# chart components


``` bash

目前元件如下：

	1. Google Map 元件 gMap.vue
  	#（使用demo 於：mapBox.vue）

	2. 匯率表單 currenyList.vue + currencySparklines.vue
 	 #（使用demo 於：platform.vue）

	3. 折線圖 lineChart.vue
 	 #（使用demo 於：linePlatform.vue & lineBarsPlatform.vue）

	4. 表單 textTable.vue
	  #（使用demo 於：linePlatform.vue）

	5. 堆疊直條圖 barChart.vue
 	 #（使用demo 於：lineBarsPlatform.vue）

	6. 離散圖 ScartterChar.vue
 	 #（使用demo 於：scatterPlatform.vue）

	7. 圓餅圖 pieChart.vue
	  #（使用demo 於：lineBarsPlatform.vue）

	8. 共用圖例控制元件 legendControl.vue

	9. 範圍選擇器 rangeInput.vue
	  #（使用demo 於：linePlatform.vue）

	10. Select 下拉選單使用：https://sagalbot.github.io/vue-select/

```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8081
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
